# Rubens Diniz, 21/10/20

# Classe ---------------------------------------------------------


class Funcionario:
    def __init__(self, codigo: int, nome: str, codigo_cargo: int):
        self.codigo = codigo
        self.nome = nome
        self.codigo_cargo = codigo_cargo


# Funcoes --------------------------------------------------------


def cadastrar_cargo(salario):
    if type(salario) == int:
        salario = float(salario)

    cargos.append(salario)
    print("Cargo (" + str(len(cargos) - 1) + ") de salario " + str(salario) + " cadastrado.")


def cadastrar_funcionario(func: Funcionario):
    erro = False

    if func.codigo_cargo >= len(cargos):
        erro = True
        print("Codigo de cargo " + str(func.codigo_cargo) + " invalido.")

    if not erro:
        for f in funcionarios:
            if f.codigo == func.codigo:
                erro = True
                print("Funcionario(a) " + func.nome + " (" + str(func.codigo) + ") nao cadastrado(a). Codigo repetido.")
                break

    if not erro:
        funcionarios.append(func)
        print("Funcionario(a) " + func.nome + " (" + str(func.codigo) + ") cadastrado(a).")


def mostrar_relatorio():
    for f in funcionarios:
        print(str(f.codigo) + " / " + str(f.nome) + " / " + str(cargos[f.codigo_cargo]))


def mostrar_pagamento_de_cargo(cargo: int):
    if cargo < len(cargos):
        total = 0

        for f in funcionarios:
            if f.codigo_cargo == cargo:
                total += cargos[cargo]

        print("O pagamento total do cargo " + str(cargo) + " é de " + str(total) + ".")
        return total
    else:
        print("Cargo inválido.")
        return 0


# Codigo ---------------------------------------------------------

cargos = []
funcionarios = []

# Dados de teste
cadastrar_cargo(2500)
cadastrar_cargo(1500)
cadastrar_cargo(10000)
cadastrar_cargo(1200)
cadastrar_cargo(800)
print()

cadastrar_funcionario(Funcionario(15, "João da Silva", 0))
cadastrar_funcionario(Funcionario(1, "Pedro Santos", 1))
cadastrar_funcionario(Funcionario(26, "Maria Oliveira", 2))
cadastrar_funcionario(Funcionario(12, "Rita Alcântara", 4))
cadastrar_funcionario(Funcionario(8, "Lígia Matos", 2))
print()

mostrar_relatorio()
print()

mostrar_pagamento_de_cargo(2)
