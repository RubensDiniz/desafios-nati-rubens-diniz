

# Funcoes --------------------------------------------------------

def adicionar_cotacao(cot: str):
    if cot != "":
        cotacoes.append(cot)
        print("Cotacao " + cot + " adicionada.")
    else:
        print("Erro em adicionar cotacao. Cotacao vazia.")


def remover_cotacao(cot: str):
    encontrada = False

    for c in cotacoes:
        if c == cot:
            cotacoes.remove(c)
            encontrada = True
            break

    if encontrada:
        print("Cotacao " + cot + " removida.")
    else:
        print("Cotacao " + cot + " nao encontrada.")


def alterar_cotacao(velho: str, novo: str):
    encontrada = False

    for i, c in enumerate(cotacoes):
        if c == velho:
            cotacoes[i] = novo
            encontrada = True
            break

    if encontrada:
        print("Cotacao " + velho + " trocada por " + novo + ".")
    else:
        print("Cotacao " + velho + " nao encontrada.")


# Codigo ---------------------------------------------------------

cotacoes = []

adicionar_cotacao("um")
adicionar_cotacao("dois")
adicionar_cotacao("tres")
adicionar_cotacao("quatro")

print(cotacoes)

remover_cotacao("dois")

print(cotacoes)

alterar_cotacao("tres", "cinco")

print(cotacoes)