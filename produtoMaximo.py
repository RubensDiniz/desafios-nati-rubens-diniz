entrada = [-6, 4, -5, 8, -10, 0, 8]
#entrada = [40, 0, -20, -10]

# Removendo todos os zeros
entrada = list(filter((0).__ne__, entrada))

# Vendo a quantidade de números negativos e, se ímpar, removendo o menor
negativos = []

for x in entrada:
    if x < 0:
        negativos.append(x)

if len(negativos) % 2 == 1:
    menor_negativo = -9999999       # Queremos saber o menor negativo ABSOLUTO...

    for x in negativos:
        if x > menor_negativo:      # ...por isso o >
            menor_negativo = x

    entrada.remove(menor_negativo)

print(entrada)