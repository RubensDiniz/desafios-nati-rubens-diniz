tabuleiro = [
    [4, 3, 2, 5, 6, 2, 3, 4],
    [1, 1, 1, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [1, 1, 1, 1, 1, 1, 1, 1],
    [4, 3, 2, 5, 6, 2, 3, 4]
]

quantidades = []

for i in range(0, 7):
    quantidades.append(0)

for linha in tabuleiro:
    for elemento in linha:
        quantidades[elemento] += 1

print("Peão: " + str(quantidades[1]) + " peça(s)")
print("Bispo: " + str(quantidades[2]) + " peça(s)")
print("Cavalo: " + str(quantidades[3]) + " peça(s)")
print("Torre: " + str(quantidades[4]) + " peça(s)")
print("Rainha: " + str(quantidades[5]) + " peça(s)")
print("Rei: " + str(quantidades[6]) + " peça(s)")