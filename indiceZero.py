# Rubens Diniz, 21/10/20

def get_maior_sequencia(array: []):
    inicio_sequencia_atual = 0
    final_sequencia_atual = 0
    inicio_melhor_sequencia = 0
    final_melhor_sequencia = 0

    na_sequencia = False

    for i, num in enumerate(array):
        if num == 1 and not na_sequencia:                         # Inicia sequencia
            na_sequencia = True
            inicio_sequencia_atual = i

        elif na_sequencia and (num == 0 or i == len(array)-1):    # Finaliza sequencia
            na_sequencia = False
            final_sequencia_atual = i-1 if num == 0 else i

            if (final_sequencia_atual - inicio_sequencia_atual) >= (final_melhor_sequencia - inicio_melhor_sequencia):
                inicio_melhor_sequencia = inicio_sequencia_atual
                final_melhor_sequencia = final_sequencia_atual

    return final_melhor_sequencia - inicio_melhor_sequencia

# ----------------------------------------------------------------


# Input do programa ----------------------------------------------
input_array = [0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1]


# Encontra os indices de 0s adjacentes a 1s
indices_possiveis = []

for i, num in enumerate(input_array):
    if num == 1:
        if i != 0 and (i-1 not in indices_possiveis):
            indices_possiveis.append(i - 1)
        if i != (len(input_array) - 1) and (i+1 not in indices_possiveis):
            indices_possiveis.append(i + 1)

# Testa todos os indices possiveis
indice_escolhido = 0

if len(indices_possiveis) != 0:
    maior_tamanho_sequencia = 0

    for indice in indices_possiveis:
        aux_array = list(input_array)
        aux_array[indice] = 1
        tamanho_sequencia_atual = get_maior_sequencia(aux_array)

        if tamanho_sequencia_atual >= maior_tamanho_sequencia:
            maior_tamanho_sequencia = tamanho_sequencia_atual
            indice_escolhido = indice

# Retorno do programa --------------------------------------------
print(indice_escolhido + 1)

